from django.urls import path

from . import views


urlpatterns = [
    path(r'', views.index, name="index"),
    path(r'<str:slug>/', views.stock_detail, name="stock_detail"),

    path(r'<str:stock_slug>/insider/', views.insiders, name="insiders"),
    path(r'<str:stock_slug>/insider/<str:insider_slug>', views.insiders, name="insiders"),

    path(r'<str:slug>/analytics', views.analytics, name="analytics"),
    path(r'<str:slug>/delta', views.delta, name="delta"),
]
