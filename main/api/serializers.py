from main.models import Stock, StockPrice, InsiderTrade
from rest_framework.serializers import ModelSerializer

class StockSerializer(ModelSerializer):
    class Meta:
        model = Stock
        fields = '__all__'


class StockPriceSerializer(ModelSerializer):
    class Meta:
        model = StockPrice
        fields = '__all__'


class InsiderTradeSerializer(ModelSerializer):
    class Meta:
        model = InsiderTrade
        fields = '__all__'
