from django.urls import path

from . import views

urlpatterns = [
    path(r'stocks/', views.StockListAPIView.as_view(), name="stock_list"),
    path(
        r'<str:slug>/', views.StockPriceListAPIView.as_view(),
        name="stock_price_list"
    ),

    path(
        r'<str:stock_slug>/insider/',
        views.InsiderTradeListAPIView.as_view(),
        name="stock_insider_trade_list"
    ),
    path(
        r'<str:stock_slug>/insider/<str:insider_slug>/',
        views.InsiderTradeListAPIView.as_view(),
        name="stock_insider_trade_list"
    ),

    path(
        r'<str:slug>/analytics',
        views.AnalyticsAPIView.as_view(),
        name="api_analytics"
    ),

    path(
        r'<str:slug>/delta/',
        views.DeltaAPIView.as_view(),
        name="api_delta"
    ),
]
