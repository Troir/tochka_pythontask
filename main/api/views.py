from rest_framework.generics import ListAPIView
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Stock, StockPrice, InsiderTrade
from main.helper import get_dates_diff, get_periods
from .serializers import (
    StockSerializer,
    StockPriceSerializer,
    InsiderTradeSerializer,
)


class StockListAPIView(ListAPIView):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer


class StockPriceListAPIView(ListAPIView):
    serializer_class = StockPriceSerializer

    def get_queryset(self):
        stock = get_object_or_404(Stock, slug=self.kwargs["slug"])
        return StockPrice.objects.filter(stock=stock)


class InsiderTradeListAPIView(ListAPIView):
    serializer_class = InsiderTradeSerializer

    def get_queryset(self):
        stock = get_object_or_404(Stock, slug=self.kwargs["stock_slug"])

        if "insider_slug" in self.kwargs:
            queryset = InsiderTrade.objects.filter(
                stock=stock,
                slug__iexact=self.kwargs["insider_slug"]
            )
        else:
            queryset = InsiderTrade.objects.filter(stock=stock)
        return queryset


class AnalyticsAPIView(APIView):
    def get(self, request, slug, format=None):
        data = get_dates_diff(
            slug, request.GET.get('date_from'),
            request.GET.get('date_to')
        )

        return Response({
            "data": data["diff"],
            "error": data["error"]
        })


class DeltaAPIView(APIView):
    def get(self, request, slug, format=None):
        data = get_periods(
            slug, request.GET.get('value'),
            request.GET.get('type')
        )
        periods = [
            StockPriceSerializer(x, many=True).data for x in data["periods"]
        ]

        return Response({
            "val_type": data["val_type"],
            "periods": periods,
            "error": data["error"]
        })
