from django.test import TestCase
from django.urls import reverse

from main.models import Stock, StockPrice, InsiderTrade

from datetime import datetime


class CommonViewTest(TestCase):
    def setUp(self):
        Stock.objects.create(title="stock", slug="stock")

    def check_view(self, view_name, template):
        s = Stock.objects.get(title="stock")
        url = reverse(view_name, kwargs={"slug": s.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template)

    def test_stock_detail_view(self):
        self.check_view("stock_detail", "main/stock.html")

    def test_analytics_view(self):
        self.check_view("analytics", "main/analytics.html")

    def test_delta_view(self):
        self.check_view("delta", "main/delta.html")


class IndexViewTest(TestCase):
    def test_stock_detail_view(self):
        url = reverse("index")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/index.html")


class InsidersViewTest(TestCase):
    def setUp(self):
        s = Stock.objects.create(title="stock", slug="stock")
        InsiderTrade.objects.create(
            stock=s, name="insider_trade", relation="relation",
            transaction_type="transaction_type", last_date=datetime.now(),
            owner_type="owner_type", shares_traded=1, last_price=1.0,
            shares_held=1, slug="insider_trade"
        )

    def test_insider_trade_view_v1(self):
        s = Stock.objects.get(title="stock")
        url = reverse("insiders", kwargs={"stock_slug": s.slug})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/insiders.html")

    def test_insider_trade_view_v2(self):
        s = Stock.objects.get(title="stock")
        it = InsiderTrade.objects.get(name="insider_trade")
        url = reverse(
            "insiders",
            kwargs={
                "stock_slug": s.slug,
                "insider_slug": it.slug
            }
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "main/insiders.html")
