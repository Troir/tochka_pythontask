from django.test import TestCase

from main.helper import get_dates_diff, get_periods
from main.models import Stock, StockPrice


class DatesDiffTest(TestCase):
    def setUp(self):
        s = Stock.objects.create(title="stock", slug="stock")
        StockPrice.objects.create(
            stock=s, date="1970-01-01", val_open=2,
            val_high=3, val_low=4, val_close=5, volume=20
        )
        StockPrice.objects.create(
            stock=s, date="1970-01-02", val_open=1,
            val_high=1, val_low=1, val_close=1, volume=10
        )

    def test_dates_diff(self):
        s = Stock.objects.get(slug="stock")
        data = get_dates_diff(s.slug, "1970-01-01", "1970-01-02")
        diff = data["diff"]
        self.assertEqual(diff["val_open"], 1)
        self.assertEqual(diff["val_high"], 2)
        self.assertEqual(diff["val_low"], 3)
        self.assertEqual(diff["val_close"], 4)

    def test_dates_diff_failed_wrong_format(self):
        s = Stock.objects.get(slug="stock")
        data = get_dates_diff(s.slug, "00-00-1970", "00-00-1970")
        self.assertEqual(data["error"]["name"], "date_wrong_format")

    def test_dates_diff_failed_no_results(self):
        s = Stock.objects.get(slug="stock")
        data = get_dates_diff(s.slug, "1970-01-03", "1970-01-04")
        self.assertEqual(data["error"]["name"], "no_dates")


class PeriodsTest(TestCase):
    def setUp(self):
        s = Stock.objects.create(title="stock", slug="stock")
        stock_prices_size = 4
        for i in range(stock_prices_size):
            val = i + 1
            StockPrice.objects.create(
                stock=s, date="1970-01-{}".format(val, "02d"), val_open=val,
                val_high=val, val_low=val,
                val_close=val, volume=val
            )

    def check_fail(self, val_diff, val_type, error_name):
        s = Stock.objects.get(slug="stock")
        data = get_periods(s.slug, val_diff, val_type)
        self.assertEqual(data["error"]["name"], error_name)

    def test_periods(self):
        val_diff = 1
        periods_expecting_count = 3
        s = Stock.objects.get(slug="stock")
        data = get_periods(s.slug, val_diff, "open")
        self.assertEqual(
            len(data["periods"]), periods_expecting_count
        )

    def test_no_periods(self):
        self.check_fail(100, "open", "no_periods")

    def test_decimal_error(self):
        self.check_fail("not a number", "open", "decimal_error")

    def test_attribute_error(self):
        self.check_fail(100, "not_existsting_type", "attribute_error")
