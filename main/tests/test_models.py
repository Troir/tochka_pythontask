from django.test import TestCase
from django.urls import reverse

from main.models import Stock, StockPrice, InsiderTrade

from datetime import datetime


class StockTest(TestCase):
    def setUp(self):
        Stock.objects.create(title="stock", slug="stock")

    def test_stock_creation(self):
        s = Stock.objects.get(title="stock")
        self.assertTrue(isinstance(s, Stock))
        self.assertEqual(s.__str__(), s.title)

    def test_absolute_url(self):
        s = Stock.objects.get(title="stock")
        self.assertTrue(isinstance(s, Stock))
        self.assertEqual(
            s.get_absolute_url(),
            reverse("stock_detail", kwargs={"slug": s.slug})
        )


class StockPriceTest(TestCase):
    def setUp(self):
        s = Stock.objects.create(title="stock", slug="stock")
        StockPrice.objects.create(
            stock=s, date=datetime.now(), val_open=1,
            val_high=2, val_low=1, val_close=3, volume=10
        )

    def test_stock_price_creation(self):
        sp = StockPrice.objects.get(
            val_open=1, val_high=2, val_low=1,
            val_close=3, volume=10
        )
        self.assertTrue(isinstance(sp, StockPrice))
        self.assertEqual(sp.__str__(), "{} {} {} {} {} {}".format(
            sp.date, sp.val_open, sp.val_high,
            sp.val_low, sp.val_close, sp.volume
        ))


class InsiderTradeTest(TestCase):
    def setUp(self):
        s = Stock.objects.create(title="stock", slug="stock")
        InsiderTrade.objects.create(
            stock=s, name="insider_trade", relation="relation",
            transaction_type="transaction_type", last_date=datetime.now(),
            owner_type="owner_type", shares_traded=1, last_price=1.0,
            shares_held=1, slug="insider_trade"
        )

    def test_insider_trade_creation(self):
        it = InsiderTrade.objects.get(name="insider_trade")
        self.assertTrue(isinstance(it, InsiderTrade))
        self.assertEqual(it.__str__(), it.name)
