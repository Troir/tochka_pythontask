from django.shortcuts import render, get_object_or_404

from .models import Stock, StockPrice
from .helper import (
    paginate, log_stacktrace,
    get_dates_diff, get_periods
)

from decimal import Decimal


def index(request):
    return render(request, "main/index.html", {
        "stocks": Stock.objects.all()
    })

def stock_detail(request, slug):
    stock = get_object_or_404(Stock, slug=slug)
    prices = paginate(request, stock.prices.all())
    return render(request, "main/stock.html", {
        "stock": stock,
        "prices": prices
    })

def insiders(request, stock_slug, insider_slug=None):
    stock = get_object_or_404(Stock, slug=stock_slug)

    trades = None
    filtered = False

    if insider_slug is None:
        trades = paginate(request, stock.insiders_trades.filter(stock=stock))
    else:
        trades = paginate(request, stock.insiders_trades.filter(
            stock=stock,
            slug__iexact=insider_slug)
        )
        filtered = True

    return render(request, "main/insiders.html", {
        "stock": stock,
        "trades": trades,
        "filtered": filtered
    })

def analytics(request, slug):
    data = get_dates_diff(
        slug, request.GET.get('date_from'),
        request.GET.get('date_to')
    )
    return render(request, "main/analytics.html", data)

def delta(request, slug):
    data = get_periods(
        slug, request.GET.get('value'),
        request.GET.get('type')
    )

    return render(request, "main/delta.html", data)
