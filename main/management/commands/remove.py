from django.core.management.base import BaseCommand
from main.models import Stock, InsiderTrade


class Command(BaseCommand):
    help = "Remove all stocks and stock prices objects, just for testing"

    def handle(self, *args, **options):
        Stock.objects.all().delete()
        InsiderTrade.objects.all().delete()
        print("Done")
