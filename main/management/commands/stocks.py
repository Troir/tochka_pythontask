from django.core.management.base import BaseCommand
from django.conf import settings

from main.parsers.stock_parser import StockParser

import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        sp = StockParser()
        sp.parse(os.path.join(
            settings.BASE_DIR, "main", "static",
            "main", "tickers.txt"
        ))
