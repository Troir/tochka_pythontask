from django.core.management.base import BaseCommand
from django.conf import settings

from main.parsers.insiders_parser import InsiderTradeParser

import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        itp = InsiderTradeParser()
        itp.parse()
