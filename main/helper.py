from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.paginator import PageNotAnInteger, EmptyPage
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator

from .models import Stock, StockPrice

from decimal import Decimal, DecimalException
import traceback
import logging

logger = logging.getLogger("main")


def log_stacktrace(e):
    stacktrace = traceback.format_exc()
    print("{}".format(stacktrace))
    print("{}".format(e))
    logger.debug("{}".format(stacktrace))
    logger.debug("{}".format(e))


def get_threads_num():
    try:
        threads_num = int(input("Enter the number of threads: "))
        if threads_num <= 0:
            print("Error! Number of threads should be a positive number")
            return get_threads_num()
        return threads_num
    except ValueError as e:
        log_stacktrace(e)


def paginate(request, objects, per_page=30):
    paginator = Paginator(objects, per_page)
    page = request.GET.get("page", "1")
    try:
        result = paginator.page(page)
    except PageNotAnInteger:
        result = paginator.page(1)
    except EmptyPage:
        result = paginator.page(paginator.num_pages)
    return result


def get_dates_diff(slug, date_from, date_to):
    data = {
        "stock": get_object_or_404(Stock, slug=slug),
        "date_from": date_from,
        "date_to": date_to,
        "error": None
    }
    if data["date_from"] and data["date_to"]:
        try:
            price_from = StockPrice.objects.get(
                date=data["date_from"], stock=data["stock"]
            )
            price_to = StockPrice.objects.get(
                date=data["date_to"], stock=data["stock"]
            )

            data["prices"] = [price_from, price_to]
            data["diff"] = {
                "val_open": price_from.val_open - price_to.val_open,
                "val_high": price_from.val_high - price_to.val_high,
                "val_low": price_from.val_low - price_to.val_low,
                "val_close": price_from.val_close - price_to.val_close
            }
        except ObjectDoesNotExist as e:
            data["error"] = {
                "name": "no_dates",
                "message": "Нет данных по этим датам"
            }
            log_stacktrace(e)
        except ValidationError as e:
            data["error"] = {
                "name": "date_wrong_format",
                "message": "Неправильный формат даты"
            }
            log_stacktrace(e)

    return data


def get_periods(slug, val, val_type):
    data = {
        "stock": get_object_or_404(Stock, slug=slug),
        "error": None
    }
    prices = data["stock"].prices.order_by("date")

    if val and val_type:
        try:
            data["val"] = Decimal(val)
            data["val_type"] = "val_{}".format(val_type)

            data["periods"] = []
            start = prices.first()
            start_idx = 0

            for idx, price in enumerate(prices.all()):
                val_start = getattr(start, data["val_type"])
                val_current = getattr(price, data["val_type"])

                if abs(val_start - val_current) >= data["val"]:
                    data["periods"].append(prices[start_idx:idx + 1])
                    start = prices[idx]
                    start_idx = idx

            if not len(data["periods"]):
                data["error"] = {
                    "name": "no_periods",
                    "message": "Нет периодов"
                }
        except AttributeError as e:
            data["error"] = {
                "name": "attribute_error",
                "message": "Неправильное значение параметра 'val_type'"
            }
            log_stacktrace(e)
        except DecimalException as e:
            data["error"] = {
                "name": "decimal_error",
                "message": "Не удалось преобразовать параметр 'val'"
            }
            log_stacktrace(e)

    return data
