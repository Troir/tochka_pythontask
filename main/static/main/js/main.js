$(document).ready(function() {
    $(".datepicker").pickadate({
        format: "yyyy-mm-dd",
        selectMonths: true,
        selectYears: 15,
    });

    $("#pricesDiffForm").on("change", function() {
        var date_from = $("#date_from").val(),
            date_to = $("#date_to").val();

        if (date_from) {
            $("#date-from-msg").removeClass("visible");
        }

        if (date_to) {
            $("#date-to-msg").removeClass("visible");
        }
    });

    $("#pricesDiffFormBtn").on("click", function(event) {
        event.preventDefault();

        var date_from = $("#date_from").val(),
            date_to = $("#date_to").val();

        if (!date_from) {
            $("#date-from-msg").addClass("visible");
            return;
        }

        if (!date_to) {
            $("#date-to-msg").addClass("visible");
            return;
        }

        $("#pricesDiffForm").submit();
    });

    $("#deltaForm").on("change", function() {
        var value = $("#value").val();

        if (value) {
            $("#value-msg").removeClass("visible");
        }
    });

    $("#deltaFormBtn").on("click", function(event) {
        event.preventDefault();

        var value = $("#value").val(),
            value_type = $("#value_type").val();

        if (!value) {
            $("#value-msg").addClass("visible");
            return;
        }

        $("#type").val($("#value_type").val());

        $("#deltaForm").submit();
    });

    $("select").material_select();
});
