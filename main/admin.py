from django.contrib import admin

from .models import Stock, StockPrice, InsiderTrade


@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):
    list_display = ("title", "slug",)
    search_fields = ("title",)
    prepopulated_fields = {"slug": ("title",)}


@admin.register(StockPrice)
class StockPriceAdmin(admin.ModelAdmin):
    list_display = (
        "stock", "date", "val_open", "val_high",
        "val_low", "val_close", "volume",
    )
    list_filter = ("stock",)


@admin.register(InsiderTrade)
class InsiderTradeAdmin(admin.ModelAdmin):
    list_display = (
        "stock", "name", "relation",
        "last_date", "transaction_type",
        "owner_type", "shares_traded",
        "last_price", "shares_held",
    )
    search_fields = (
        "name", "transaction_type", "owner_type",
    )
    prepopulated_fields = {"slug": ("name",)}
