from django.urls import reverse
from django.db import models

_charfield_length = 40

class Stock(models.Model):
    title = models.CharField("название", max_length=_charfield_length)
    slug = models.SlugField("ссылка", unique=True)

    class Meta:
        verbose_name = "акция"
        verbose_name_plural = "акции"
        ordering = ["title"]

    def __str__(self):
        return "{}".format(self.title)

    def get_absolute_url(self):
        return reverse("stock_detail", kwargs={"slug": self.slug})


class StockPrice(models.Model):
    stock = models.ForeignKey(
        Stock, verbose_name="акция", related_name="prices",
        on_delete=models.CASCADE
    )
    date = models.DateField("дата")

    max_digits = 20
    decimal_places = 4
    val_open = models.DecimalField(
        "откр.", max_digits=max_digits, decimal_places=decimal_places
    )
    val_high = models.DecimalField(
        "макс.", max_digits=max_digits, decimal_places=decimal_places
    )
    val_low = models.DecimalField(
        "мин.", max_digits=max_digits, decimal_places=decimal_places
    )
    val_close = models.DecimalField(
        "послед.", max_digits=max_digits, decimal_places=decimal_places
    )
    volume = models.IntegerField("объём")

    class Meta:
        verbose_name = "цена акции"
        verbose_name_plural = "цены акций"
        ordering = ["stock__title", "-date"]

    def __str__(self):
        return "{} {} {} {} {} {}".format(
            self.date, self.val_open, self.val_high,
            self.val_low, self.val_close, self.volume
        )


class InsiderTrade(models.Model):
    stock = models.ForeignKey(
        Stock, related_name="insiders_trades",
        on_delete=models.CASCADE
    )

    name = models.CharField("имя", max_length=_charfield_length)
    relation = models.CharField("связь", max_length=_charfield_length)
    last_date = models.DateField("дата последней операции")
    transaction_type = models.CharField("тип операции", max_length=_charfield_length)
    owner_type = models.CharField("тип владельца", max_length=_charfield_length)
    shares_traded = models.IntegerField("проданные акции")
    last_price = models.DecimalField(
        "цена последней операции",
        max_digits=10, decimal_places=4,
        blank=True, null=True
    )
    shares_held = models.IntegerField("акции, находящиеся в собственности")
    slug = models.SlugField("ссылка")

    class Meta:
        verbose_name = "данные о торговле совладельца"
        verbose_name_plural = "данные о торговле совладельцев"
        ordering = ["name", "-last_date"]

    def __str__(self):
        return "{}".format(self.name)
