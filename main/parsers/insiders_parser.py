from django.db.utils import OperationalError, DatabaseError
from django.template.defaultfilters import slugify
from django.db import connection

from main.helper import log_stacktrace, get_threads_num
from main.models import Stock, InsiderTrade

from psycopg2 import ProgrammingError
from multiprocessing import Pool
from datetime import datetime
from bs4 import BeautifulSoup
from decimal import Decimal
import requests
import timeit
import os


class InsiderTradeParser(object):
    def __init__(self):
        super(InsiderTradeParser, self).__init__()

        self.batch_size = 1000

    def _get_count_of_pages(self, soup_obj):
        last_page = soup_obj.find("a", {"id": "quotes_content_left_lb_LastPage"})
        page_count = int(last_page["href"].split("=")[1])
        if page_count > 10:
            page_count = 10

        return page_count

    def _create_insider_trade(self, stock, values):
        last_date_str = values[2].contents[0].replace("\r\n", "").strip()
        last_date = datetime.strptime(last_date_str, '%m/%d/%Y')
        last_price = Decimal(values[6].string) if values[6].string else 0

        price = InsiderTrade(
            stock=stock,
            name=values[0].string,
            relation=values[1].string,
            last_date=last_date,
            transaction_type=values[3].string,
            owner_type=values[4].string,
            shares_traded=int(values[5].string.replace(',', '')),
            last_price=last_price,
            shares_held=int(values[7].string.replace(',', '')),
            slug=slugify(values[0].string)
        )
        return price

    def _parse_page(self, stock, url, page_number, soup_obj):
        if page_number > 1:
            url = "{}?page={}".format(url, page_number)
            html_content = requests.get(url).text
            soup_obj = BeautifulSoup(html_content, "html.parser")

        table = soup_obj.find("div", {"class": "genTable"})
        insiders = []
        for tr in table.find_all("tr"):
            values = tr.find_all("td")
            if len(values):
                insiders.append(self._create_insider_trade(stock, values))

        try:
            InsiderTrade.objects.bulk_create(insiders, self.batch_size)
        except (DatabaseError, ProgrammingError) as e:
            log_stacktrace(e)

    def _parse_insider(self, stock):
        url = "http://www.nasdaq.com/symbol/{}/insider-trades".format(stock.title.lower())
        html_content = requests.get(url).text
        soup_obj = BeautifulSoup(html_content, "html.parser")

        page_count = self._get_count_of_pages(soup_obj)

        for page_number in range(1, page_count + 1):
            self._parse_page(stock, url, page_number, soup_obj)

    def parse(self):
        stocks = Stock.objects.all()
        if not stocks.count():
            print("No stocks in database, abort...")
            return

        threads_num = get_threads_num()

        print("Parsing insiders...")

        start = timeit.default_timer()

        pool = Pool(threads_num)
        pool.map(self._parse_insider, stocks)
        pool.close()
        pool.join()

        end = timeit.default_timer()
        print("Wasted time: {}".format(end - start))
