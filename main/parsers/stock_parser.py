from django.db.utils import OperationalError, DatabaseError
from django.template.defaultfilters import slugify
from django.db import connection

from main.helper import log_stacktrace, get_threads_num
from main.models import Stock, StockPrice

from psycopg2 import ProgrammingError
from multiprocessing import Pool
from datetime import datetime
from bs4 import BeautifulSoup
from decimal import Decimal
import requests
import timeit
import os


class StockParser(object):
    def __init__(self):
        super(StockParser, self).__init__()

        self.batch_size = 1000

    def _get_stocks(self, filepath):
        try:
            if os.path.getsize(filepath) == 0:
                print("File empty, nothing to parse, abort...")
                return

            stocks_list = []
            with open(filepath, "r") as f:
                for line in f:
                    stock = line.replace("\n", "").strip()
                    if not stock:
                        print("Empty line, pass...")
                        continue

                    new_stock, created = Stock.objects.get_or_create(
                        title=stock, slug=slugify(stock)
                    )
                    if created:
                        stocks_list.append(new_stock)
                        print("Successfully created new stock '{}'".format(stock))
                    else:
                        print("Stock '{}' already exists in database, pass...".format(stock))

            return stocks_list
        except (FileNotFoundError, OSError) as e:
            log_stacktrace(e)

    def _create_stock_price(self, stock, values):
        date_str = values[0].contents[0].replace("\r\n", "").strip()
        date = datetime.strptime(date_str, '%m/%d/%Y')

        price = StockPrice(
            stock=stock,
            date=date,
            val_open=Decimal(values[1].string),
            val_high=Decimal(values[2].string),
            val_low=Decimal(values[3].string),
            val_close=Decimal(values[4].string),
            volume=int(values[5].string.replace(',', ''))
        )
        return price

    def _parse_stock(self, stock):
        print("Parsing prices for stock '{}'...".format(stock.title))

        url = "http://www.nasdaq.com/symbol/{}/historical".format(stock.title.lower())
        html_content = requests.get(url).text

        soup = BeautifulSoup(html_content, "html.parser")
        table = soup.find("div", {"id": "historicalContainer"})

        """
            NOTE: Начинаем парсинг с третьего элемента,
                  поскольку первые два на сайте скрыты
                  и не содержат информации по ценам акций
        """
        skip_elements = 2
        stocks = []
        for tr in table.find_all("tr")[skip_elements:]:
            values = tr.find_all("td")
            if len(values):
                stocks.append(self._create_stock_price(stock, values))

        try:
            StockPrice.objects.bulk_create(stocks, self.batch_size)
        except (DatabaseError, ProgrammingError) as e:
            log_stacktrace(e)

    def parse(self, filepath):
        stocks = self._get_stocks(filepath)
        threads_num = get_threads_num()

        if len(stocks) and threads_num:
            try:
                start = timeit.default_timer()

                pool = Pool(threads_num)
                pool.map(self._parse_stock, stocks)
                pool.close()
                pool.join()

                end = timeit.default_timer()
                print("Wasted time: {}".format(end - start))
            except OperationalError as e:
                log_stacktrace(e)
