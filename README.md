В проекте используется версия python = 3.5.2

Для развёртывания проекта нужно:

1) Создать виртуальное окружение

2) Установить зависимости, указанные в файле requiremnts.txt (pip install -r requirements.txt)

3) Создать базу и пользователя в postgresql (взять реквизиты, что указаны в settings.py или указать свои)
    Название базы: tochka_python_task
    Пользователь: tochka
    Пароль: 123

4) Применить миграции:
    python manage.py migrate

5) Команда для парсинга акций:
    python manage.py stocks

6) Команда для парсинга данных о совладельцах:
    python manage.py insiders

7) Запус проекта:
    python manage.py runserver (http://127.0.0.1:8000)
